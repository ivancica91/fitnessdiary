﻿using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database
{
    public interface INutrientRepository
    {
        Task<Nutrient?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);

        Task<Nutrient> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default);

        Task<List<Nutrient>> GetAsync(string? name, Page page, CancellationToken cancellationToken = default);

        void Create(Nutrient nutrient);

        void Update(Nutrient nutrient);

        void Delete(Nutrient nutrient);

    }
}
