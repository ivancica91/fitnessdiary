﻿using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database
{
    public interface IMealCategoryRepository 
    {
        Task<MealCategory?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);

        Task<MealCategory> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default);
        Task<MealCategory?> GetByNameAsync(string name, CancellationToken cancellationToken = default);  

        Task<List<MealCategory>> GetAsync(string? name, Page page, CancellationToken cancellationToken = default);

        void Create(MealCategory mealCat);

        void Update(MealCategory mealCat);

        void Delete(MealCategory mealCat);
    }
}
