﻿namespace CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database
{
    using CogentCode.FitnessDiary.Domain;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork
    {
        IMealRepository Meals { get; }
        IMealCategoryRepository MealCategories { get; }
        INutrientRepository Nutrients { get; }
        Task SaveAsync(CancellationToken cancellationToken = default);
    }
}