﻿using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database
{
    public interface IMealRepository
    {
        Task<Meal?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);

        Task<Meal> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default);
        Task<Meal?> GetByNameAsync(string name, CancellationToken cancellationToken = default);

        Task<List<Meal>> GetAsync(string? name, Page page, CancellationToken cancellationToken = default);

        void Create(Meal meal);

        void Update(Meal meal);

        void Delete(Meal meal);

    }
}
