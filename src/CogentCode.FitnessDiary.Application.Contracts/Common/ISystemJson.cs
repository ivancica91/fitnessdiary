﻿namespace CogentCode.FitnessDiary.Application.Contracts.Common
{
    using System;

    public interface ISystemJson
    {
        public T Deserialize<T>(string json);

        public object? Deserialize(string json, Type type);

        public string Serialize(object obj);
    }
}