﻿namespace CogentCode.FitnessDiary.Application.Contracts.Common
{
    using System;

    public interface ISystemClock
    {
        DateTimeOffset Utc => DateTimeOffset.UtcNow;

        long EpochSeconds => DateTimeOffset.UtcNow.ToUnixTimeSeconds();
    }
}