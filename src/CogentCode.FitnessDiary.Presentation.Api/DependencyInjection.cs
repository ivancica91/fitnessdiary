﻿namespace CogentCode.FitnessDiary.Presentation.Api
{
    using Microsoft.Extensions.DependencyInjection;

    using Swashbuckle.AspNetCore.Filters;

    using System.Reflection;

    public static class DependencyInjection
    {
        public static IServiceCollection AddRestApi(this IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();

            services.AddSwaggerExamplesFromAssemblies(assembly);

            return services;
        }
    }
}
