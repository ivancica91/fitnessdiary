﻿namespace CogentCode.FitnessDiary.Hub.Presentation.Api.Controllers.V1
{
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Commands;
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Commands;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Queries;
    using CogentCode.FitnessDiary.Presentation.Api.Common.Constants;
    using CogentCode.FitnessDiary.Presentation.Api.Controllers;

    using MediatR;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    using Swashbuckle.AspNetCore.Annotations;

    using System.Net.Mime;
    using System.Threading.Tasks;

    [ApiVersion(ApiVersions.V1)]
    public class NutrientsController : ApiControllerBase
    {
        public NutrientsController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        [SwaggerOperation(OperationId = nameof(GetNutrients), Tags = new[] { ApiTags.Nutrients })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetNutrients([FromQuery] GetNutrientsListQuery query)
        {
            return await this.ProcessAsync(query);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = nameof(GetNutrient), Tags = new[] { ApiTags.Nutrients })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetNutrient(Guid id)
        {
            return await this.ProcessAsync(new GetNutrientByIdQuery(id));
        }

        [HttpPost]
        [SwaggerOperation(OperationId = nameof(PostNutrient), Tags = new[] { ApiTags.Nutrients })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostNutrient([FromBody] AddNutrientCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpPut]
        [SwaggerOperation(OperationId = nameof(PutNutrient), Tags = new[] { ApiTags.Nutrients })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutNutrient([FromBody] UpdateNutrientCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(OperationId = nameof(DeleteNutrient), Tags = new[] { ApiTags.Nutrients })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteNutrient(Guid id)
        {
            return await this.ProcessAsync(new DeleteNutrientCommand(id));
        }
    }
}