﻿namespace CogentCode.FitnessDiary.Hub.Presentation.Api.Controllers.V1
{
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Commands;
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Commands;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Queries;
    using CogentCode.FitnessDiary.Presentation.Api.Common.Constants;
    using CogentCode.FitnessDiary.Presentation.Api.Controllers;

    using MediatR;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    using Swashbuckle.AspNetCore.Annotations;

    using System.Net.Mime;
    using System.Threading.Tasks;

    [ApiVersion(ApiVersions.V1)]
    public class MealsController : ApiControllerBase
    {
        public MealsController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        [SwaggerOperation(OperationId = nameof(GetMeals), Tags = new[] { ApiTags.Meals })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetMeals([FromQuery] GetMealsListQuery query)
        {
            return await this.ProcessAsync(query);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = nameof(GetMeal), Tags = new[] { ApiTags.Meals })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetMeal(Guid id)
        {
            return await this.ProcessAsync(new GetMealByIdQuery(id));
        }

        [HttpPost]
        [SwaggerOperation(OperationId = nameof(PostMeal), Tags = new[] { ApiTags.Meals })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostMeal([FromBody] AddMealCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpPut]
        [SwaggerOperation(OperationId = nameof(PutMeal), Tags = new[] { ApiTags.Meals })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutMeal([FromBody] UpdateMealCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(OperationId = nameof(DeleteMeal), Tags = new[] { ApiTags.Meals })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteMeal(Guid id)
        {
            return await this.ProcessAsync(new DeleteMealCommand(id));
        }
    }
}