﻿namespace CogentCode.FitnessDiary.Hub.Presentation.Api.Controllers.V1
{
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Commands;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Queries;
    using CogentCode.FitnessDiary.Presentation.Api.Common.Constants;
    using CogentCode.FitnessDiary.Presentation.Api.Controllers;

    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    using Swashbuckle.AspNetCore.Annotations;

    using System.Net.Mime;
    using System.Threading.Tasks;

    [ApiVersion(ApiVersions.V1)]
    public class MealCategoryController : ApiControllerBase
    {
        public MealCategoryController(IMediator mediator) : base(mediator)
        {
        }
        [Authorize]
        [HttpGet]
        [SwaggerOperation(OperationId = nameof(GetMealsCategories), Tags = new[] { ApiTags.MealCategories })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetMealsCategories([FromQuery] GetMealsCategoryListQuery query)
        {
            return await this.ProcessAsync(query);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(OperationId = nameof(GetMealCategory), Tags = new[] { ApiTags.MealCategories })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetMealCategory(Guid id)
        {
            return await this.ProcessAsync(new GetMealCategoryByIdQuery(id));
        }

        [HttpPost]
        [SwaggerOperation(OperationId = nameof(PostMealCategory), Tags = new[] { ApiTags.MealCategories })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostMealCategory([FromBody] AddMealCategoryCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpPut]
        [SwaggerOperation(OperationId = nameof(PutMealCategory), Tags = new[] { ApiTags.MealCategories })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutMealCategory([FromBody] UpdateMealCategoryCommand command)
        {
            return await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(OperationId = nameof(DeleteMealCategory), Tags = new[] { ApiTags.MealCategories })]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteMealCategory(Guid id)
        {
            return await this.ProcessAsync(new DeleteMealCategoryCommand(id));
        }
    }
}