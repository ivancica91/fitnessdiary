﻿using CogentCode.FitnessDiary.Application.Contracts.Identity;
using CogentCode.FitnessDiary.Application.Models;
using Microsoft.AspNetCore.Mvc;
 
namespace CogentCode.FitnessDiary.Presentation.Api.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        public AccountController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("authenticate")]
        public async Task<ActionResult<AuthenticationResponse>> AuthenticateAsync(AuthenticationRequest request)
        {
            return Ok(await _authenticationService.AuthenticateAsync(request));
        }
    }

}

