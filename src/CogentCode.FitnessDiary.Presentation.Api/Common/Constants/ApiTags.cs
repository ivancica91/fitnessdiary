﻿namespace CogentCode.FitnessDiary.Presentation.Api.Common.Constants
{
    internal static class ApiTags
    {
        public const string Meals = "Meals";
        public const string MealCategories = "MealCategories";
        public const string Nutrients = "Nutrients";

    }
}