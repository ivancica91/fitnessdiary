namespace CogentCode.FitnessDiary
{
    using CogentCode.FitnessDiary.Application.Contracts.Identity;
    using CogentCode.FitnessDiary.Common.Constants;
    using CogentCode.FitnessDiary.Infrastructure.Identity.Models;
    using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Hosting;
    using Serilog;

    public class Program
    {
        public async static Task<int> Main(string[] args)
        {
            var builder = new ConfigurationBuilder();

            BuildConfiguration(builder, args);

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Build())
                .CreateBootstrapLogger();

            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var loggerFactory = services.GetRequiredService<ILoggerFactory>();

                try
                {
                    var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();

                    await Infrastructure.Identity.Seed.UserCreator.SeedAsync(userManager);

                    Log.Information("Starting web host.");
                    CreateHostBuilder(args).Build().Run();
                    return ExitCode.Success;
                }
                catch (Exception exception)
                {
                    Log.Fatal(exception, "Host terminated unexpectedly.");
                    return ExitCode.Failure;
                }
                finally
                {
                    Log.CloseAndFlush();
                }


            }


        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .UseSerilog((hostContext, loggerConfiguration) =>
                    loggerConfiguration.ReadFrom.Configuration(hostContext.Configuration)
                )
                .ConfigureWebHostDefaults(webBuilder =>
                    webBuilder.UseStartup<Startup>()
                );

        private static void BuildConfiguration(IConfigurationBuilder builder, string[] args)
        {
            var environment = Environment.GetEnvironmentVariable(HostEnviroment.Variable);

            var isDevelopment =
                string.IsNullOrWhiteSpace(environment)
                || string.Equals(environment, HostEnviroment.Development, StringComparison.OrdinalIgnoreCase);

            builder
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: false,
                    reloadOnChange: true)
                .AddJsonFile(
                    path: $"appsettings.{environment}.json",
                    optional: true,
                    reloadOnChange: true)
                .AddEnvironmentVariables();

            if (isDevelopment)
            {
                builder.AddUserSecrets<Program>();
            }

            builder.AddCommandLine(args);
        }
    }
}