﻿namespace CogentCode.FitnessDiary.Common.Utilities
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Versioning;

    internal static class ApiVersioningSetup
    {
        public static void Specification(ApiVersioningOptions options)
        {
            options.DefaultApiVersion = ApiVersion.Default;
            options.AssumeDefaultVersionWhenUnspecified = true;
            options.ReportApiVersions = true;
        }
    }
}