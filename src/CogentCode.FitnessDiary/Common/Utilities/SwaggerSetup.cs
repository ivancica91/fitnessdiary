﻿namespace CogentCode.FitnessDiary.Common.Utilities
{
    using CogentCode.FitnessDiary.Common.ApiDocumentation;
    using CogentCode.FitnessDiary.Common.Constants;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.Filters;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using Swashbuckle.AspNetCore.SwaggerUI;

    using System;
    using System.IO;

    internal static class SwaggerSetup
    {
        public static void DocsSpecification(SwaggerGenOptions options)
        {
            options.EnableAnnotations();
            options.ExampleFilters();
            options.OperationFilter<SwaggerDefaultValues>();



            foreach (var xmlFile in Directory.GetFiles(AppContext.BaseDirectory, "*.xml"))
            {
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                options.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
            }

            options.SwaggerDoc("v1", new OpenApiInfo { Title = "MyAPI", Version = "v1" });
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please enter token",
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                Scheme = "bearer"
            });
            options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type=ReferenceType.SecurityScheme,
                            Id="Bearer"
                        }
                    },
                    new string[]{}
                }
            });
        }



        public static void UiSpecification(SwaggerUIOptions options, IApiVersionDescriptionProvider descriptionProvider)
        {
            options.DisplayOperationId();
            options.DisplayRequestDuration();
            options.RoutePrefix = string.Empty;

            foreach (ApiVersionDescription description in descriptionProvider.ApiVersionDescriptions)
            {
                options.SwaggerEndpoint(
                    url: Api.GetDocumentationUrl(description.GroupName),
                    name: description.GroupName.ToUpperInvariant());
            }
        }
    }
}