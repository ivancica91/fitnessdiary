﻿namespace CogentCode.FitnessDiary.Common.Constants
{
    internal static class ExitCode
    {
        public const int Success = 0;
        public const int Failure = 1;
    }
}