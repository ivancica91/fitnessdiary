﻿namespace CogentCode.FitnessDiary.Common.Constants
{
    internal static class Api
    {
        public const string Title = "Fitness Diary API";

        public const string GroupNameFormat = "'v'VV";

        public static string GetDocumentationUrl(string version)
        {
            return $"/swagger/{version}/swagger.json";
        }
    }
}