﻿namespace CogentCode.FitnessDiary.Common.Constants
{
    internal static class HealthCheck
    {
        public const string DisplayName = "Health checks.";
        public const string Endpoint = "/health";
    }
}