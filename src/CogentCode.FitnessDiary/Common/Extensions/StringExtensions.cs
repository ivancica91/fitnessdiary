﻿namespace CogentCode.FitnessDiary.Common.Extensions
{
    using System;

    internal static class StringExtensions
    {
        internal static string EscapeNewLine(this string value, string substitue = "\\r\\n")
        {
            return value.Replace(Environment.NewLine, substitue);
        }
    }
}