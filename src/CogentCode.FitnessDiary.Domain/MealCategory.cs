﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Domain
{
    public class MealCategory
    {
        public Guid Id { get; init; }
        public long Created { get; init; }
        public string Name { get; protected set; } = string.Empty;
        public ICollection<Meal> Meals { get; protected set; } = new List<Meal>();
        protected MealCategory()
        {
        }

        public MealCategory(Guid id, long created, string name)
        {
            this.Id = id;
            this.Created = created;
            this.Name = name;
        }

        public void UpdateMealCategory(string name)
        {
            this.Name = name;
        }

        public void AddMeal(Meal meal)
            => this.Meals.Add(meal);

        public void RemoveMeal(string name)
        {
            Meal? meal = this.GetMeal(name);
            if (meal != null)
                this.Meals.Remove(meal);
        }

        public void UpdateMeal(Meal meal, Meal oldMeal)
        {
            Meal? m = this.GetMeal(oldMeal.Name);
            m?.UpdateMeal(meal.Name, meal.Calories, meal.Protein, meal.Carbs, meal.Fats);
        }

        private Meal? GetMeal(string name)
            => this.Meals.Where(n => n.Name == name).SingleOrDefault();  
    }
}
