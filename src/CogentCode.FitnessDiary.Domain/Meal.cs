﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Domain
{
    public class Meal
    {
        public Guid Id { get; init; }
        public long Created { get; init; }
        public string Name { get; protected set; } = string.Empty;
        public decimal Calories { get; protected set; } = decimal.Zero;
        public decimal Protein { get; protected set; } = decimal.Zero;
        public decimal Carbs { get; protected set; } = decimal.Zero;
        public decimal Fats { get; protected set; } = decimal.Zero;
        public int? CategoryID { get; protected set; }
        public MealCategory? MealCategory { get; protected set; }
        public ICollection<Nutrient> Nutrients { get; protected set; } = new List<Nutrient>();

        protected Meal() { }

        public Meal(Guid id, long created, string name, decimal calories, decimal protein, decimal carbs, decimal fats, int? categoryID)
        {
            this.Id = id;
            this.Created = created;
            this.Name = name;  
            this.Calories = calories;
            this.Protein = protein;
            this.Carbs = carbs;
            this.Fats = fats;
            this.CategoryID = categoryID;
        }

        public void UpdateMeal(string name, decimal calories, decimal protein, decimal carbs, decimal fats)
        {
            this.Name = name;
            this.Calories = calories;
            this.Protein = protein;
            this.Carbs = carbs;
            this.Fats = fats;
        }

        public void AddNutrient(Nutrient nutrient)
            => this.Nutrients.Add(nutrient);

        public void UpdateNutrient(Nutrient nutrient, Nutrient oldNutrient)
        {
            Nutrient? nut = this.GetNutrient(oldNutrient.Name);
            nut?.UpdateNutrient(nutrient.Name, nutrient.Calories, nutrient.Carbs, nutrient.Protein, nutrient.Fats);
        }
        public void RemoveNutrient(string name)
        {
            Nutrient? nut = this.GetNutrient(name);

            if (nut != null)
            this.Nutrients.Remove(nut);
        }

        public void ChangeMealCategory(int mealCategory)
            => this.CategoryID = mealCategory;

        public Nutrient? GetNutrient(string name)
            => this.Nutrients.Where(n => n.Name == name).SingleOrDefault();




    }

}
