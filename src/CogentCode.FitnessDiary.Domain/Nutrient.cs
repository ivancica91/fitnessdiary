﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Domain
{
    public class Nutrient
    {
        public Guid Id { get; init; }
        public long Created { get; init; }
        public string Name { get; protected set; } = String.Empty;
        public decimal Calories { get; protected set; } = decimal.Zero;
        public decimal Protein { get; protected set; } = decimal.Zero;
        public decimal Carbs { get; protected set; } = decimal.Zero;
        public decimal Fats { get; protected set; } = decimal.Zero;

        protected Nutrient()
        {
        }

        public Nutrient(Guid id, long created, string name,  decimal calories, decimal protein, decimal carbs, decimal fats)
        {
            this.Id = id;  
            this.Created = created;
            this.Name = name;
            this.Calories = calories;
            this.Protein = protein;
            this.Carbs = carbs;
            this.Fats = fats;

        }
        public void UpdateNutrient(string name, decimal calories, decimal protein, decimal carbs, decimal fats)
        {
            this.Name = name;
            this.Calories = calories;
            this.Protein = protein;
            this.Carbs = carbs; 
            this.Fats = fats;
        }

    }
}
