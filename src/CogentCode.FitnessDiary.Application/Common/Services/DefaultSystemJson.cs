﻿namespace CogentCode.FitnessDiary.Application.Common.Services
{

    using CogentCode.FitnessDiary.Application.Contracts.Common;

    using System;

    public class DefaultSystemJson : ISystemJson
    {
        public T Deserialize<T>(string json)
        {
            return System.Text.Json.JsonSerializer.Deserialize<T>(json) ?? throw new NullReferenceException("Unable to deserialize json.");
        }

        public object? Deserialize(string json, Type type)
        {
            return System.Text.Json.JsonSerializer.Deserialize(json, type);
        }

        public string Serialize(object obj)
        {
            return System.Text.Json.JsonSerializer.Serialize(obj);
        }
    }
}
