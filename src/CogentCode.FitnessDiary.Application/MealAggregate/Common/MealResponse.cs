﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Common
{
    public class MealResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = default!;
        public decimal Calories { get; protected set; }
        public decimal Protein { get; protected set; } 
        public decimal Carbs { get; protected set; } 
        public decimal Fats { get; protected set; } 
        public int? CategoryID { get; protected set; }
    }
}
