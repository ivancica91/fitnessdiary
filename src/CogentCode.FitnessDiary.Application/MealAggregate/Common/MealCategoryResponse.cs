﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Common
{
    using System;

    public class MealCategoryResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;

    }
}
