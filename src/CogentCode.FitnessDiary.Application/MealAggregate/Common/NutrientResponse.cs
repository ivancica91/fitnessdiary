﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Common
{
    public class NutrientResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
    }
}
