﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading.Tasks;

    public record UpdateNutrientCommand(Guid Id, string Name, decimal calories, decimal protein, decimal carbs, decimal fats) : IRequest;

    internal sealed class UpdateNutrientValidator : AbstractValidator<UpdateNutrientCommand>
    {
        public UpdateNutrientValidator()
        {
            this.RuleFor(r => r.Name)
                .NotEmpty()
                .MaximumLength(20);
        }
    }

    internal sealed class UpdateNutrientCommandHandler : AsyncRequestHandler<UpdateNutrientCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateNutrientCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(UpdateNutrientCommand request, CancellationToken cancellationToken)
        {
            Nutrient nutrient = await this._unitOfWork.Nutrients.GetByIdSafeAsync(request.Id);

            nutrient.UpdateNutrient(request.Name, request.calories, request.protein, request.carbs, request.fats);

            this._unitOfWork.Nutrients.Update(nutrient);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
