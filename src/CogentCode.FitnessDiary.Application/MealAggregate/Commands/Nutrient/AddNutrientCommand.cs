﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System.Threading;
    using System.Threading.Tasks;

    public record AddNutrientCommand(string Name, decimal calories, decimal protein, decimal  carbs, decimal  fats) : IRequest;

    internal sealed class AddNutrientCommandValidator : AbstractValidator<AddNutrientCommand>
    {
        public AddNutrientCommandValidator()
        {
            this.RuleFor(r => r.Name)
               .MaximumLength(20)
                .NotEmpty();
        }
    }

    internal sealed class AddNutrientCommandHandler : AsyncRequestHandler<AddNutrientCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISystemClock _systemClock;

        private readonly ISystemGuid _systemGuid;

        public AddNutrientCommandHandler(
            IUnitOfWork unitOfWork,
            ISystemClock systemClock,
            ISystemGuid systemGuid)
        {
            this._unitOfWork = unitOfWork;
            this._systemClock = systemClock;
            this._systemGuid = systemGuid;
        }

        protected override async Task Handle(AddNutrientCommand request, CancellationToken cancellationToken)
        {
            Nutrient nutrient = new Nutrient(
                this._systemGuid.Create(),
                this._systemClock.EpochSeconds,
                request.Name,
                request.calories,
                request.protein,
                request.fats,
                request.carbs);

            this._unitOfWork.Nutrients.Create(nutrient);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
