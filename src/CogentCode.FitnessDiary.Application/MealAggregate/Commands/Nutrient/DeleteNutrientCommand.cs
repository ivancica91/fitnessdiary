﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record DeleteNutrientCommand(Guid Id) : IRequest;

    internal sealed class DeleteNutrientCommandValidator : AbstractValidator<DeleteNutrientCommand>
    {
        public DeleteNutrientCommandValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    internal sealed class DeleteNutrientCommandHandler : AsyncRequestHandler<DeleteNutrientCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteNutrientCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(DeleteNutrientCommand request, CancellationToken cancellationToken)
        {
            Nutrient nutrient = await this._unitOfWork.Nutrients.GetByIdSafeAsync(request.Id, cancellationToken);

            this._unitOfWork.Nutrients.Delete(nutrient);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
