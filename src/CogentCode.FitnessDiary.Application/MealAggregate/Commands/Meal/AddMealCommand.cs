﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System.Threading;
    using System.Threading.Tasks;

    public record AddMealCommand(string Name, decimal calories, decimal protein, decimal  carbs, decimal  fats, int? categoryId ) : IRequest;

    internal sealed class AddMealCommandValidator : AbstractValidator<AddMealCommand>
    {
        public AddMealCommandValidator()
        {
            this.RuleFor(r => r.Name)
               .MaximumLength(20)
                .NotEmpty();
        }
    }

    internal sealed class AddMealCommandHandler : AsyncRequestHandler<AddMealCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISystemClock _systemClock;

        private readonly ISystemGuid _systemGuid;

        public AddMealCommandHandler(
            IUnitOfWork unitOfWork,
            ISystemClock systemClock,
            ISystemGuid systemGuid)
        {
            this._unitOfWork = unitOfWork;
            this._systemClock = systemClock;
            this._systemGuid = systemGuid;
        }

        protected override async Task Handle(AddMealCommand request, CancellationToken cancellationToken)
        {
            Meal meal = new Meal(
                this._systemGuid.Create(),
                this._systemClock.EpochSeconds,
                request.Name,
                request.calories,
                request.fats,
                request.carbs,
                request.protein,
                request.categoryId);

            this._unitOfWork.Meals.Create(meal);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
