﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record DeleteMealCommand(Guid Id) : IRequest;

    internal sealed class DeleteMealCommandValidator : AbstractValidator<DeleteMealCommand>
    {
        public DeleteMealCommandValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    internal sealed class DeleteMealCommandHandler : AsyncRequestHandler<DeleteMealCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteMealCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(DeleteMealCommand request, CancellationToken cancellationToken)
        {
            Meal meal = await this._unitOfWork.Meals.GetByIdSafeAsync(request.Id, cancellationToken);

            this._unitOfWork.Meals.Delete(meal);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
