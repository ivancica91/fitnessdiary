﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading.Tasks;

    public record UpdateMealCommand(Guid Id, string Name, decimal calories, decimal protein, decimal carbs, decimal fats, int? CategoryID) : IRequest;

    internal sealed class UpdateMealValidator : AbstractValidator<UpdateMealCommand>
    {
        public UpdateMealValidator()
        {
            this.RuleFor(r => r.Name)
                .NotEmpty()
                .MaximumLength(20);
        }
    }

    internal sealed class UpdateMealCommandHandler : AsyncRequestHandler<UpdateMealCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateMealCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(UpdateMealCommand request, CancellationToken cancellationToken)
        {
            Meal meal = await this._unitOfWork.Meals.GetByIdSafeAsync(request.Id);

            meal.UpdateMeal(request.Name, request.calories, request.protein, request.carbs, request.fats);

            this._unitOfWork.Meals.Update(meal);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
