﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System.Threading;
    using System.Threading.Tasks;

    public record AddMealCategoryCommand(string Name) : IRequest;

    internal sealed class AddMealCategoryCommandValidator : AbstractValidator<AddMealCategoryCommand>
    {
        public AddMealCategoryCommandValidator()
        {
            this.RuleFor(r => r.Name)
               .MaximumLength(20)
                .NotEmpty();
        }
    }

    internal sealed class AddMealCategoryCommandHandler : AsyncRequestHandler<AddMealCategoryCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ISystemClock _systemClock;

        private readonly ISystemGuid _systemGuid;

        public AddMealCategoryCommandHandler(
            IUnitOfWork unitOfWork,
            ISystemClock systemClock,
            ISystemGuid systemGuid)
        {
            this._unitOfWork = unitOfWork;
            this._systemClock = systemClock;
            this._systemGuid = systemGuid;
        }

        protected override async Task Handle(AddMealCategoryCommand request, CancellationToken cancellationToken)
        {
            MealCategory mealCategory = new MealCategory(
                this._systemGuid.Create(),
                this._systemClock.EpochSeconds,
                request.Name);

            this._unitOfWork.MealCategories.Create(mealCategory);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
