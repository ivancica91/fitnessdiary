﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record DeleteMealCategoryCommand(Guid Id) : IRequest;

    internal sealed class DeleteMealCategoryCommandValidator : AbstractValidator<DeleteMealCategoryCommand>
    {
        public DeleteMealCategoryCommandValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    internal sealed class DeleteMealCategoryCommandHandler : AsyncRequestHandler<DeleteMealCategoryCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteMealCategoryCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(DeleteMealCategoryCommand request, CancellationToken cancellationToken)
        {
            MealCategory mealCategory = await this._unitOfWork.MealCategories.GetByIdSafeAsync(request.Id, cancellationToken);

            this._unitOfWork.MealCategories.Delete(mealCategory);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
