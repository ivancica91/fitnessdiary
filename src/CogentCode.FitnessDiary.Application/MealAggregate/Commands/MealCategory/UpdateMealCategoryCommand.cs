﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Commands
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading.Tasks;

    public record UpdateMealCategoryCommand(Guid Id, string Name) : IRequest;

    internal sealed class UpdateMealCategoryValidator : AbstractValidator<UpdateMealCategoryCommand>
    {
        public UpdateMealCategoryValidator()
        {
            this.RuleFor(r => r.Name)
                .NotEmpty()
                .MaximumLength(20);
        }
    }

    internal sealed class UpdateMealCategoryCommandHandler : AsyncRequestHandler<UpdateMealCategoryCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateMealCategoryCommandHandler(
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        protected override async Task Handle(UpdateMealCategoryCommand request, CancellationToken cancellationToken)
        {
            MealCategory mealCategory = await this._unitOfWork.MealCategories.GetByIdSafeAsync(request.Id);

            mealCategory.UpdateMealCategory(request.Name);

            this._unitOfWork.MealCategories.Update(mealCategory);

            await this._unitOfWork.SaveAsync(cancellationToken);
        }
    }
}
