﻿using AutoMapper;
using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
using CogentCode.FitnessDiary.Application.TodoAggregate.Common;
using CogentCode.FitnessDiary.Domain;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries
{
    public record GetMealsCategoryListQuery(string? Name, int? PageIndex, int? PageSize) : IRequest<List<MealCategoryResponse>>;
    internal sealed class GetMealsCategoryListQueryValidator : AbstractValidator<GetMealsCategoryListQuery>
    {
    }

    internal sealed class GetMealsCategoryListQueryHandler : IRequestHandler<GetMealsCategoryListQuery, List<MealCategoryResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetMealsCategoryListQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<List<MealCategoryResponse>> Handle(GetMealsCategoryListQuery request, CancellationToken cancellationToken)
        {
            Page page = new(request.PageIndex, request.PageSize);

            List<MealCategory> categories = await this._unitOfWork.MealCategories.GetAsync(request.Name, page);

            return this._mapper.Map<List<MealCategoryResponse>>(categories);
        }
    }

}
