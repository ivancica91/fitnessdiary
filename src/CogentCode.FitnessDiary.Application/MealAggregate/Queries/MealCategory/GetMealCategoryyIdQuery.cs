﻿namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries
{
    using AutoMapper;

    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Application.TodoAggregate.Common;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record class GetMealCategoryByIdQuery(Guid Id) : IRequest<MealCategoryResponse>;

    public class GetMealCategoryByIdQueryValidator : AbstractValidator<GetMealCategoryByIdQuery>
    {
        public GetMealCategoryByIdQueryValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    public class GetMealCategoryByIdQueryHandler : IRequestHandler<GetMealCategoryByIdQuery, MealCategoryResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetMealCategoryByIdQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<MealCategoryResponse> Handle(GetMealCategoryByIdQuery request, CancellationToken cancellationToken)
        {
            MealCategory mealCategories = await this._unitOfWork.MealCategories.GetByIdSafeAsync(request.Id, cancellationToken);

            return this._mapper.Map<MealCategoryResponse>(mealCategories);
        }
    }
}
