﻿using AutoMapper;
using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Common;
using CogentCode.FitnessDiary.Domain;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries
{
    public record GetNutrientsListQuery(string? Name, int? PageIndex, int? PageSize) : IRequest<List<NutrientResponse>>;
    internal sealed class GetNutrientsListQueryValidator : AbstractValidator<GetNutrientsListQuery>
    {
    }

    internal sealed class GetNutrientsListQueryHandler : IRequestHandler<GetNutrientsListQuery, List<NutrientResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetNutrientsListQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<List<NutrientResponse>> Handle(GetNutrientsListQuery request, CancellationToken cancellationToken)
        {
            Page page = new(request.PageIndex, request.PageSize);

            List<Nutrient> Nutrients = await this._unitOfWork.Nutrients.GetAsync(request.Name, page);

            return this._mapper.Map<List<NutrientResponse>>(Nutrients);
        }
    }
}
