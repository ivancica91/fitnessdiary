﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Queries
{
    using AutoMapper;

    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Common;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record class GetNutrientByIdQuery(Guid Id) : IRequest<NutrientResponse>;

    public class GetNutrientByIdQueryValidator : AbstractValidator<GetNutrientByIdQuery>
    {
        public GetNutrientByIdQueryValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    public class GetNutrientByIdQueryHandler : IRequestHandler<GetNutrientByIdQuery, NutrientResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetNutrientByIdQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<NutrientResponse> Handle(GetNutrientByIdQuery request, CancellationToken cancellationToken)
        {
            Nutrient nutrient = await this._unitOfWork.Nutrients.GetByIdSafeAsync(request.Id, cancellationToken);

            return this._mapper.Map<NutrientResponse>(nutrient);
        }
    }
}
