﻿namespace CogentCode.FitnessDiary.Application.TodoAggregate.Queries
{
    using AutoMapper;

    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
    using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Common;
    using CogentCode.FitnessDiary.Domain;

    using FluentValidation;

    using MediatR;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public record class GetMealByIdQuery(Guid Id) : IRequest<MealResponse>;

    public class GetMealByIdQueryValidator : AbstractValidator<GetMealByIdQuery>
    {
        public GetMealByIdQueryValidator()
        {
            this.RuleFor(r => r.Id)
                .NotEmpty();
        }
    }

    public class GetMealByIdQueryHandler : IRequestHandler<GetMealByIdQuery, MealResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetMealByIdQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<MealResponse> Handle(GetMealByIdQuery request, CancellationToken cancellationToken)
        {
            Meal meal = await this._unitOfWork.Meals.GetByIdSafeAsync(request.Id, cancellationToken);

            return this._mapper.Map<MealResponse>(meal);
        }
    }
}
