﻿using AutoMapper;
using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
using CogentCode.FitnessDiary.Application.MealCategoryAggregate.Common;
using CogentCode.FitnessDiary.Domain;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.MealCategoryAggregate.Queries
{
    public record GetMealsListQuery(string? Name, int? PageIndex, int? PageSize) : IRequest<List<MealResponse>>;
    internal sealed class GetMealsListQueryValidator : AbstractValidator<GetMealsListQuery>
    {
    }

    internal sealed class GetMealsListQueryHandler : IRequestHandler<GetMealsListQuery, List<MealResponse>>
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetMealsListQueryHandler(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<List<MealResponse>> Handle(GetMealsListQuery request, CancellationToken cancellationToken)
        {
            Page page = new(request.PageIndex, request.PageSize);

            List<Meal> meals = await this._unitOfWork.Meals.GetAsync(request.Name, page);

            return this._mapper.Map<List<MealResponse>>(meals);
        }
    }
}
