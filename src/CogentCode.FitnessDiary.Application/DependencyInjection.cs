﻿namespace CogentCode.FitnessDiary.Application
{
    using CogentCode.FitnessDiary.Application.Common.Behaviors;
    using CogentCode.FitnessDiary.Application.Common.Services;
    using CogentCode.FitnessDiary.Application.Contracts.Common;
    using FluentValidation;

    using MediatR;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.Extensions.DependencyInjection;

    using System.Reflection;

    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            var applicationAssembly = Assembly.GetExecutingAssembly();

            services.AddValidatorsFromAssemblies(new[] { applicationAssembly }, ServiceLifetime.Scoped, null, true);

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));


            services.AddMediatR(applicationAssembly);

            services.AddScoped<ISystemJson, DefaultSystemJson>();

            services.AddScoped<ISystemGuid, DefaultSystemGuid>();

            services.AddScoped<ISystemClock, DefaultSystemClock>();

            return services;
        }
    }
}
