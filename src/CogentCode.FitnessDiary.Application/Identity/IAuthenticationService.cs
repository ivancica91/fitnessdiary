﻿using CogentCode.FitnessDiary.Application.Models;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Application.Contracts.Identity
{
    public interface IAuthenticationService 
    { 
         Task<AuthenticationResponse> AuthenticateAsync(AuthenticationRequest request);
    }
}
