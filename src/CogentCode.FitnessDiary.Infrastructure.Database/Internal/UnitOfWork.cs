﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.Internal
{
    using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;

    using System.Threading;
    using System.Threading.Tasks;

    internal sealed class UnitOfWork : IUnitOfWork
    {
        private readonly FitnessDiaryDbContext _context;
        public INutrientRepository Nutrients { get; }
        public IMealRepository Meals { get; }
        public IMealCategoryRepository MealCategories { get; }

        public UnitOfWork( FitnessDiaryDbContext context,INutrientRepository nutrients,IMealCategoryRepository mealCategories,IMealRepository meals)
        {
            this._context = context;
            this.Nutrients = nutrients;
            this.Meals = meals;
            this.MealCategories = mealCategories;
        }


        public async Task SaveAsync(CancellationToken cancellationToken = default)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}