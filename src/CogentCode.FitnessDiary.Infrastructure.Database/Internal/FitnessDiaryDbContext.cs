﻿namespace CogentCode.FitnessDiary.Infrastructure.Database.Internal
{
    using CogentCode.FitnessDiary.Application.Contracts;
    using Microsoft.EntityFrameworkCore;

    using System.Reflection;

    internal class FitnessDiaryDbContext : DbContext
    {
        //private readonly ILoggedInUserService _loggedInUserService;

        public FitnessDiaryDbContext(DbContextOptions<FitnessDiaryDbContext> options) : base(options)
        {
        }
    //    public FitnessDiaryDbContext(DbContextOptions<FitnessDiaryDbContext> options, ILoggedInUserService loggedInUserService)
    //: base(options)
    //    {
    //        _loggedInUserService = loggedInUserService;
    //    }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
