﻿using CogentCode.FitnessDiary.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Infrastructure.Database.Configurations
{
    internal class MealEntityTypeConfiguration : IEntityTypeConfiguration<Meal>
    {
        // ovdje rucno u projektu za svaki class izradujemo tablicu?
        public void Configure(EntityTypeBuilder<Meal> builder)
        {
            builder.ToTable("meals");

            builder.HasKey(t => t.Id);

            builder.Property(p => p.Name).IsRequired();

            builder.Property(p => p.Calories).IsRequired();

            builder.Property(p => p.Protein).IsRequired();

            builder.Property(p => p.Carbs).IsRequired();

            builder.Property(p => p.Fats).IsRequired();

            builder.Property(p => p.CategoryID);

            builder.Property(p => p.Created).IsRequired();
        }
    }
}
