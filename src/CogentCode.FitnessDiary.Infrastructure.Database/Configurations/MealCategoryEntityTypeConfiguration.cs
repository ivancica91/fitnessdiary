﻿using CogentCode.FitnessDiary.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Infrastructure.Database.Configurations
{
    internal class MealCategoryEntityTypeConfiguration : IEntityTypeConfiguration<MealCategory>
    {
        public void Configure(EntityTypeBuilder<MealCategory> builder)
        {
            builder.ToTable("mealCategories");

            builder.HasKey(t => t.Id);

            builder.Property(p => p.Name).IsRequired();

            builder.Property(p => p.Created).IsRequired();
        }
    }


}
