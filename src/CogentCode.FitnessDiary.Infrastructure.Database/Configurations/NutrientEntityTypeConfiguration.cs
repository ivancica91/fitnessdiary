﻿using CogentCode.FitnessDiary.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Infrastructure.Database.Configurations
{
    internal class NutrientEntityTypeConfiguration : IEntityTypeConfiguration<Nutrient>
    {
        public void Configure(EntityTypeBuilder<Nutrient> builder)
        {
            builder.ToTable("nutrients");

            builder.HasKey(t => t.Id);

            builder.Property(p => p.Name).IsRequired();

            builder.Property(p => p.Calories).IsRequired();

            builder.Property(p => p.Protein).IsRequired();

            builder.Property(p => p.Carbs).IsRequired();

            builder.Property(p => p.Fats).IsRequired();

            builder.Property(p => p.Created).IsRequired();
        }
    }

}
