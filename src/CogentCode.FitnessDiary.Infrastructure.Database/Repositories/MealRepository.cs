﻿using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
using CogentCode.FitnessDiary.Domain;
using CogentCode.FitnessDiary.Infrastructure.Database.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Infrastructure.Database.Repositories
{
    internal sealed class MealRepository : IMealRepository
    {
        private readonly DbSet<Meal> _meals;

        public MealRepository(FitnessDiaryDbContext context)
        {
            this._meals = context.Set<Meal>();
        }
        public void Create(Meal meal)
        {
            this._meals.Add(meal);
        }

        public void Delete(Meal meal)
        {
            this._meals.Remove(meal);
        }

        public async Task<List<Meal>> GetAsync(string? name, Page page, CancellationToken cancellationToken = default)
        {
            return await this._meals
                .WhereIf(name is not null, mealCat => mealCat.Name.StartsWith(name ?? string.Empty))
                .Skip(page.Skip)
                .Take(page.Take)
                .ToListAsync(cancellationToken);

        }

        public async Task<Meal?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await this._meals.SingleOrDefaultAsync(mealCat => mealCat.Id == id, cancellationToken);
        }

        public async Task<Meal> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await this.GetByIdAsync(id, cancellationToken) ?? throw new ApplicationException("Not found.");
        }

        public async Task<Meal?> GetByNameAsync(string name, CancellationToken cancellationToken = default)
        {
            return await this._meals.SingleOrDefaultAsync(meal => meal.Name == name)?? throw new ApplicationException("Not found");
        }

        public void Update(Meal meal)
        {
            this._meals.Update(meal);
        }
    }
}
