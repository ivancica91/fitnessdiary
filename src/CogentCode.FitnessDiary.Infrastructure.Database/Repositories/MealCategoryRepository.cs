﻿using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
using CogentCode.FitnessDiary.Domain;
using CogentCode.FitnessDiary.Infrastructure.Database.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Infrastructure.Database.Repositories
{
    internal sealed class MealCategoryRepository : IMealCategoryRepository
    {
        private readonly DbSet<MealCategory> _mealCategories;

        public MealCategoryRepository(FitnessDiaryDbContext context)
        {
            this._mealCategories = context.Set<MealCategory>();
        }

        public void Create(MealCategory mealCat)
        {
            this._mealCategories.Add(mealCat);  
        }

        public void Delete(MealCategory mealCat)
        {
            this._mealCategories.Remove(mealCat);
        }

        public async Task<List<MealCategory>> GetAsync(string? name, Page page, CancellationToken cancellationToken = default)
        {
            return await this._mealCategories
                .WhereIf(name is not null, mealCat => mealCat.Name.StartsWith(name ?? string.Empty))
                .Skip(page.Skip)
                .Take(page.Take)
                .ToListAsync(cancellationToken);
        }

        public async Task<MealCategory?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
           return await this._mealCategories.SingleOrDefaultAsync(mealCat => mealCat.Id == id, cancellationToken);  
        }

        public async Task<MealCategory> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await GetByIdAsync(id, cancellationToken) ?? throw new ApplicationException("Not found.");
        }

        public async Task<MealCategory?> GetByNameAsync(string name, CancellationToken cancellationToken = default)
        {
            return await this._mealCategories.SingleOrDefaultAsync(mealCat => mealCat.Name == name, cancellationToken) ?? throw new ApplicationException("Not found.");
        }

        public void Update(MealCategory mealCat)
        {
            this._mealCategories.Update(mealCat);
        }
    }
}
