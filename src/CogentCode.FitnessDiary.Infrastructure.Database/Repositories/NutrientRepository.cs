﻿using CogentCode.FitnessDiary.Application.Contracts.Common;
using CogentCode.FitnessDiary.Application.Contracts.Infrastructure.Database;
using CogentCode.FitnessDiary.Domain;
using CogentCode.FitnessDiary.Infrastructure.Database.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogentCode.FitnessDiary.Infrastructure.Database.Repositories
{
    internal sealed class NutrientRepository : INutrientRepository
    {
        private readonly DbSet<Nutrient> _nutrients;

        public NutrientRepository(FitnessDiaryDbContext context)
        {
            this._nutrients = context.Set<Nutrient>();
        }
        public void Create(Nutrient nutrient)
        {
            this._nutrients.Add(nutrient);
        }

        public void Delete(Nutrient nutrient)
        {
            this._nutrients.Remove(nutrient);
        }

        public async Task<List<Nutrient>> GetAsync(string? name, Page page, CancellationToken cancellationToken = default)
        {
            return await this._nutrients
                .WhereIf(name is not null, nutrientt => nutrientt.Name.StartsWith(name ?? string.Empty))
                .Skip(page.Skip)
                .Take(page.Take)
                .ToListAsync(cancellationToken);

        }

        public async Task<Nutrient?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await this._nutrients.SingleOrDefaultAsync(nutrient => nutrient.Id == id, cancellationToken);
        }

        public async Task<Nutrient> GetByIdSafeAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await this.GetByIdAsync(id, cancellationToken) ?? throw new ApplicationException("Not found.");
        }

        public async Task<Nutrient?> GetByNameAsync(string name, CancellationToken cancellationToken = default)
        {
            return await this._nutrients.SingleOrDefaultAsync(nutrient => nutrient.Name == name) ?? throw new ApplicationException("Not found");
        }

        public void Update(Nutrient nutrient)
        {
            this._nutrients.Update(nutrient);
        }
    }
}
