#### Development environment setup

- Download and install [Visual Studio 2022 Community](https://visualstudio.microsoft.com/vs/community/)

#### [User secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows)
When working locally, sensitive information needed to run the project is stored in the *secrets.json* file at `%APPDATA%\Microsoft\UserSecrets\<user_secrets_id>\secrets.json`.
On the different deployment environments that configuration is defined through the environment variables of the *web.config* file.

If you're running the project for the first time locally, ask someone to give you all the data you need and add it to your *secrets.json* file.

1. Right click on `CogentCode.FitnessDiary`
2. Select `Manage User Secrets`
3. Add relevant secrets:

```
{
	"DatabaseSettings:ConnectionString": "[your connection string]"
}
```

#### [Database migrations](https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli)
EF Core code first migrations are used to build the database for the local and development environment.

When executing the migrations command using `Package Manager Console` tools, make sure you selected `Default Project`: `src\CogentCode.FitnessDiary.Infrastructure.Database` and run:
```
Add-Migration "[migration name]"
```

Update the database to the latest migration:
```
Update-Database
```